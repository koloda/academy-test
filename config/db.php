<?php
$dbPath = realpath(__DIR__ . '/../data/academy.db');

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:' . $dbPath,
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
