<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "walktrough".
 *
 * @property integer $id
 * @property string $sessid
 * @property string $login
 * @property string $start_time
 * @property string $end_time
 * @property integer $current_step
 * @property integer $s0
 * @property integer $s1
 * @property integer $s2
 * @property integer $s3
 * @property integer $s4
 * @property integer $s5
 * @property integer $s6
 * @property integer $points
 */
class Walktrough extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'walktrough';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['login', 'sessid'], 'string' ],
            [['s1', 's2', 's3', 's4', 's5', 's6', 's0', 'current_step',  'points', 'start_time', 'end_time'], 'number']
        ];
    }

    public static function primaryKey() {
        return ['sessid'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'sessid' => 'Session ID',
            'login' => 'Login',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'current_step' => 'Current Step',
            's1' => 'S1',
            's2' => 'S2',
            's3' => 'S3',
            's4' => 'S4',
            's5' => 'S5',
            's6' => 'S6',
            's7' => 'S7',
            'points' => 'Points',
        ];
    }

    public static function my($sessid) {
        $w = self::findOne(['sessid' => $sessid]);

        if (!$w) {
            $w = new Walktrough();
            $w->sessid = $sessid;
            $w->start_time = time();
            $w->current_step = 0;
            $w->points = 0;
            $w->save();
        }

        return $w;
    }

    public function getCurrentAction() {
        $action = 'step_' . $this->current_step;

        return Url::toRoute($action);
    }

    public function completeStep0() {
        $this->points = 1;
        $this->s0 = 1;
        $this->current_step = $this->getNextStep();
        $this->save();

        Yii::$app->session->setFlash('message', 'First step completed successful!', false);

        return;
    }

    public function completeStep1() {
        $this->points++;
        $this->s1 = 1;
        $this->current_step = $this->getNextStep();
        $this->save();

        Yii::$app->session->setFlash('message', 'Text step completed successful!', false);

        return;
    }

    public function completeStep2($ansver) {
        $a = Yii::$app->session->get('a');
        $b = Yii::$app->session->get('b');

        if ((int)$a + (int)$b == (int)$ansver) {
            $this->points++;
            Yii::$app->session->setFlash('message', 'Math step completed successful!', false);
        } else {
            Yii::$app->session->setFlash('error', 'Math step completed with error.', false);
        }

        $this->s2 = 1;
        $this->current_step = $this->getNextStep();
        $this->save();

        Yii::$app->session->remove('a');
        Yii::$app->session->remove('b');

        return;
    }

    public function completeStep3($langs) {
        Yii::$app->session->setFlash('error', 'Radio step completed bad.', false);

        if (count($langs)) {
            if (!in_array('vb', $langs)) {
                $this->points++;

                Yii::$app->session->setFlash('message', 'Radio step completed successful.', false);
                Yii::$app->session->removeFlash('error');
            }
        }

        $this->s3 = 1;
        $this->current_step = $this->getNextStep();
        $this->save();

        return;
    }

    public function completeStep4($day) {
        if ($day == date('N')) {
            $this->points++;
           Yii::$app->session->setFlash('message', 'Check step completed successful.', false);
        } else {
            Yii::$app->session->setFlash('error', 'Check step completed with error.', false);
        }

        $this->s4 = 1;
        $this->current_step = $this->getNextStep();
        $this->save();

        return;
    }

    public function completeStep5($status) {
        if ($status === '0') {
            $this->points++;
            Yii::$app->session->setFlash('message', 'Video step completed successful.', false);
        } else {
            Yii::$app->session->setFlash('error', 'Video step completed with error.', false);
        }

        $this->s5 = 1;
        $this->current_step = $this->getNextStep();
        $this->save();

        return;
    }

    public function completeStep6() {
        $this->s6 = 1;
        $this->end_time = time();
        $this->save();

        return;
    }

    private function getNextStep() {
        $freeSteps = [];
        for ($i=0; $i<6; $i++) {
            $k = "s$i";
            if ($this->attributes[$k] != 1) {
                $freeSteps[] = $i;
            }
        }

        if (!count($freeSteps)) {
            return 6;
        } else {
            $index = rand(0, count($freeSteps) - 1);
            return $freeSteps[$index];
        }
    }
}
