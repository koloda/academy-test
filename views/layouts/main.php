<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use yii\bootstrap\BaseHtml;

AppAsset::register($this);
$session = Yii::$app->session;
$session->open();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Super Academy Course',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => 'Terminate current walktrough',
                'url' => ['/site/terminate']
            ]
        ],
    ]);

    NavBar::end();
    ?>

    <div class="container">
    	<?php if($session->hasFlash('error')):?>
    	<div class="alert alert-warning">
    	<p><b>Error:</b> <?= $session->getFlash('error') ?></p>
    	</div>
    	<?php endif;?>

    	<?php if($session->hasFlash('message')):?>
    	<div class="alert alert-success">
    	<p><b>Success:</b> <?= $session->getFlash('message') ?></p>
    	</div>
    	<?php endif;?>

        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Koloda-Test <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
