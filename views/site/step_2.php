<?php

use yii\bootstrap\BaseHtml;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Math</h1>

        <p class="lead">Calculate this</p>
    </div>

    <div class="container center">
        <?= BaseHtml::beginForm('', 'post', ['class' => 'col-lg-6 col-lg-offset-3']) ?>

		<?= BaseHtml::textInput('', "$a + $b", ['class' => 'form-control text-center', 'disabled' => 'disabled'])?>
		<span class="text-center">=</span>
		<?= BaseHtml::input('number', 'ansver', '', ['class' => 'form-control text-center', 'placeholder' => 'Type ansver here', 'required' => 'required'])?>
		<br />

        <?= BaseHtml::submitButton('Next ->', ['class' => 'btn btn-lg btn-success']) ?>

        <?= BaseHtml::endForm() ?>
    </div>
</div>
