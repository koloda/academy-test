<?php

use yii\bootstrap\BaseHtml;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Read</h1>

        <p class="lead">Just read the text</p>
    </div>

    <div class="container center">
        <?= BaseHtml::beginForm('', 'post', ['class' => 'col-lg-6 col-lg-offset-3']) ?>

		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi atque illum consequuntur non aspernatur nisi quidem et velit deserunt modi dolorem adipisci quod accusamus? Ab aliquid architecto hic ullam minima!</p>
		<br />

        <?= BaseHtml::submitButton('Next ->', ['class' => 'btn btn-lg btn-success']) ?>

        <?= BaseHtml::endForm() ?>
    </div>
</div>
