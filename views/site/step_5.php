<?php
use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

	<div class="jumbotron">
		<h1>Video</h1>

		<p class="lead">Yo!</p>
	</div>

	<div class="container center">
        <?= Html::beginForm('', 'post')?>

		<div id="player"></div>
		<?= Html::input('hidden', 'status', -1, ['id' => 'input-status'])?>
		<br />

        <?= Html::submitButton('Next ->', ['class' => 'btn btn-lg btn-success'])?>

        <?= Html::endForm()?>
    </div>
</div>

<script>
  var tag = document.createElement('script');

  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      height: '390',
      width: '640',
      videoId: 'xh_LJIreB40',
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
  }

  function onPlayerStateChange(event) {
	  var input = document.getElementById('input-status');
	  input.value = event.data;
  }

  function onPlayerReady(event) {
    event.target.playVideo();
  }
</script>


