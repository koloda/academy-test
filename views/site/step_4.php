<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Radio</h1>

        <p class="lead">Check current day of week</p>
    </div>

    <div class="container center">
        <?= Html::beginForm('', 'post', ['class' => 'col-lg-6 col-lg-offset-3']) ?>

		<?= Html::radioList('day', [], $days, ['class' => 'form-group checkbox'])?>
		<br />

        <?= Html::submitButton('Next ->', ['class' => 'btn btn-lg btn-success']) ?>

        <?= Html::endForm() ?>
    </div>
</div>
