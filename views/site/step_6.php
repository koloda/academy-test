<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations, <?= $w->login?>!</h1>

        <p class="lead">You are finished with <b><?= $w->points?> points</b> during <b><?= ($w->end_time - $w->start_time)?> seconds</b>.</p>
    </div>

    <div class="container center">
		<div class="col-lg-6 col-lg-offset-3">
			<?= Html::a('Go to Start again', Url::toRoute('site/terminate'), ['class' => 'btn btn-lg btn-info'])?>
		</div>
    </div>
</div>
