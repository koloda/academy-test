<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Check</h1>

        <p class="lead">Check programming languages you know</p>
    </div>

    <div class="container center">
        <?= Html::beginForm('', 'post', ['class' => 'col-lg-6 col-lg-offset-3']) ?>

		<?= Html::checkboxList('langs', [], $langs, ['class' => 'form-group checkbox'])?>
		<br />

        <?= Html::submitButton('Next ->', ['class' => 'btn btn-lg btn-success']) ?>

        <?= Html::endForm() ?>
    </div>
</div>
