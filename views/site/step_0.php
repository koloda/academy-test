<?php

use yii\bootstrap\BaseHtml;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\base\Widget;
use app\models\Walktrough;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Hi there!</h1>

        <p class="lead">Now you can start this course. </p>
    </div>

    <div class="container center">
        <?= BaseHtml::beginForm('', 'post', ['class' => 'col-lg-6 col-lg-offset-3']) ?>

		<?php if($walk->login): ?>
			<h4>Previous course walktroughs</h4>

			<?php $dataProvider = new ActiveDataProvider([
                'query' => Walktrough::find()
			         ->where(['>', 'end_time', 0])
			         ->orderBy('end_time', 'DESC')
		             ->limit(10)
			         ->offset(0),
			    'pagination' => [
                    'pageSize' => 10
			     ]
            ]);

			echo GridView::widget([
			    'dataProvider'   => $dataProvider,
			    'columns'    => [
                    'login',
			        'points',
			        [
			            'attribute' => 'Time spent',
			            'value' => function($data) {
			                 $seconds = $data->end_time - $data->start_time;

    	                     $mins = (int)($seconds/60);
    	                     $secs = $seconds%60;

		                     return "$mins:$secs";
			             }
			         ]
                ]
			]);
			?>
		<?php else: ?>
        	<?= BaseHtml::textInput('login', '', ['placeholder' => 'Please type your login', 'required' => 'required', 'class' => 'form-control']) ?>
        <?php endif;?>
        <br>

        <?= BaseHtml::submitButton('Next ->', ['class' => 'btn btn-lg btn-success']) ?>

        <?= BaseHtml::endForm() ?>
    </div>
</div>
