<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\models\Walktrough;
use yii\helpers\ArrayHelper;

class SiteController extends Controller {

    public $defaultAction = 'step_0';

    /**
     * @var Walktrough
     */
    private $walktrough;

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init() {
        Yii::$app->session->open();
        $this->walktrough = Walktrough::my(Yii::$app->session->id);

        return true;
    }

    private function checkStep($step) {
        if ($step != $this->walktrough->current_step) {
            $this->redirect($this->walktrough->getCurrentAction());
        }
    }

    /**
     * Displays start page or current step (if not 0).
     *
     * @return string
     */
    public function actionStep_0() {
        $this->checkStep(0);

        if (Yii::$app->request->isPost) {
            if ($this->walktrough->login) {
                $this->walktrough->completeStep0();
                $this->redirect($this->walktrough->getCurrentAction());
            } else {
                $login = Yii::$app->request->post('login');

                if (!$login) {
                    Yii::$app->session->setFlash('error', 'Please fill the "Login" field.');
                    $this->refresh();
                } else {
                    $this->walktrough->login = $login;
                    $this->walktrough->save();
                }
            }
        }

        return $this->render('step_0', ['walk' => $this->walktrough]);
    }

    public function actionStep_1() {
        $this->checkStep(1);

        if (Yii::$app->request->isPost) {
            $this->walktrough->completeStep1();

            $this->redirect($this->walktrough->getCurrentAction());
        }

        return $this->render('step_1');
    }

    public function actionStep_2() {
        $this->checkStep(2);

        if (Yii::$app->request->isPost) {
            $ansver = Yii::$app->request->post('ansver');
            $this->walktrough->completeStep2($ansver);

            $this->redirect($this->walktrough->getCurrentAction());
        }

        $a = rand(1, 50);
        $b = rand(1, 50);

        Yii::$app->session->set('a', $a);
        Yii::$app->session->set('b', $b);

        return $this->render('step_2', ['a' => $a, 'b' => $b]);
    }

    public function actionStep_3() {
        $this->checkStep(3);

        if (Yii::$app->request->isPost) {
            $langs = Yii::$app->request->post('langs');
            $this->walktrough->completeStep3($langs);

            $this->redirect($this->walktrough->getCurrentAction());
        }

        $langs = [
            'php'       => 'PHP',
            'python'    => 'Python',
            'js'        => 'JavaScript',
            'csharp'    => 'C#',
            'vb'        => 'Visual Basic'
        ];

        return $this->render('step_3', ['langs' => $langs]);
    }

    public function actionStep_4() {
        $this->checkStep(4);

        if (Yii::$app->request->isPost) {
            $day = Yii::$app->request->post('day');
            $this->walktrough->completeStep4($day);

            $this->redirect($this->walktrough->getCurrentAction());
        }

        $allDays = [
            '1' => 'Monday',
            '2' => 'Tuesday',
            '3' => 'Wednesday',
            '4' => 'Thursday',
            '5' => 'Friday',
            '6' => 'Saturday',
            '7' => 'Sunday'
        ];
        $currDay = date('N');
        $days = [
            $currDay => $allDays[$currDay]
        ];

        while (count($days) < 4) {
            $d = rand(1, 7);
            if (!isset($days[$d])) {
                $days[$d] = $allDays[$d];
            }
        }
        $days = $this->shuffle_assoc($days);

        return $this->render('step_4', ['days' => $days]);
    }

    public function actionStep_5() {
        $this->checkStep(5);

        if (Yii::$app->request->isPost) {
            $status = Yii::$app->request->post('status');
            $this->walktrough->completeStep5($status);

            $this->redirect($this->walktrough->getCurrentAction());
        }

        return $this->render('step_5');
    }

    public function actionStep_6() {
        $this->checkStep(6);
        $this->walktrough->completeStep6();

        return $this->render('step_6', ['w' => $this->walktrough]);
    }

    public function actionTerminate() {
        if ($this->walktrough->end_time > 0) {
            Yii::$app->session->regenerateID();
        } else {
            $this->walktrough->delete();
        }

        $this->redirect(Url::toRoute('/'));
    }

    private function shuffle_assoc($list) {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }

        return $random;
    }
}
